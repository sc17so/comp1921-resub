#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

struct studentRecord
{
  int studentId;
  char firstName[20];
  char lastName[20];
  char password[20];
};

struct bookRecord
{
  int bookId;
  char title[50];
  char author[40];
  char genre[20];
  char year[4];
  int state; //o=available, 1=loaned, 2=deleted
};

const int BOOK_AVAILABLE = 0;
const int BOOK_UNAVAILABLE = 1;
const int BOOK_DELETED = 2;


struct loanRecord
{
  int studentId;
  int bookId;
  bool returned;
};

int getNextBookId()
{
  struct bookRecord currentBook;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb")) == NULL){
      return 1;
  }
  fseek(fptr, -sizeof(struct bookRecord),SEEK_END);
  fread(&currentBook, sizeof(struct bookRecord), 1, fptr);
  fclose(fptr);
  return currentBook.bookId+1;
}
int getNextStudentId()
{
  struct studentRecord currentStudent;
  FILE *fptr;

  if ((fptr = fopen("students.bin","rb")) == NULL){
      return 1;
  }
  fseek(fptr, -sizeof(struct studentRecord),SEEK_END);
  fread(&currentStudent, sizeof(struct studentRecord), 1, fptr);
  fclose(fptr);
  return currentStudent.studentId+1;
}

// add a book record to the library
void addBook()
{

  struct bookRecord newBook;
  FILE *fptr;

  // get all the book data from console e.g.
  printf("please enter the title:\n");
  scanf("%s",newBook.title);
  printf("please enter the author:\n");
  scanf("%s",newBook.author);
  printf("please enter the genre:\n");
  scanf("%s",newBook.genre);
  printf("please enter the year of publication:\n");
  scanf("%s",newBook.year);

  newBook.bookId=getNextBookId();
  newBook.state=BOOK_AVAILABLE;

  // append the book data to the books file
  if ((fptr = fopen("books.bin","ab")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }



  fwrite(&newBook, sizeof(struct bookRecord), 1, fptr);
  fclose(fptr);
}

// remove a book from thje library
void deleteBook(int id)
{
  struct bookRecord currentBook;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb+")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if(currentBook.bookId==id)
    {
      currentBook.state=BOOK_DELETED;
      fseek(fptr, -sizeof(struct bookRecord),SEEK_CUR);
      fwrite(&currentBook, sizeof(struct bookRecord), 1, fptr);
      fflush(fptr);
      printf("\n*** Deleted book %d, %s ***\n\n", id,currentBook.title);
    };
  }
  fclose(fptr);
}

// register a new student
void addStudent()
{
  struct studentRecord newStudent;
  FILE *fptr;
  // get all the book data from console e.g.
  printf("please enter the students First Name:\n");
  scanf("%s",newStudent.firstName);
  printf("please enter the students last Name:\n");
  scanf("%s",newStudent.lastName);
  printf("please enter the New Password:\n");
  scanf("%s",newStudent.password);
  newStudent.studentId=getNextStudentId();

  // append the book data to the books file
  if ((fptr = fopen("students.bin","ab")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  fwrite(&newStudent, sizeof(struct studentRecord), 1, fptr);
  fclose(fptr);
}

// assign a book to a student
void loanBook(int student, int book)
{
    struct loanRecord newLoan;
    struct bookRecord currentBook;
    struct studentRecord currentStudent;
    FILE *fptr;

  newLoan.studentId = student;
  newLoan.bookId = book;
  newLoan.returned = false;

  if ((fptr = fopen("loans.bin","ab")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }
  fwrite(&newLoan, sizeof(struct loanRecord), 1, fptr);
  fclose(fptr);

  if ((fptr = fopen("books.bin","rb+")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if(currentBook.bookId==book)
    {
      currentBook.state=BOOK_UNAVAILABLE;
      fseek(fptr, -sizeof(struct bookRecord),SEEK_CUR);
      fwrite(&currentBook, sizeof(struct bookRecord), 1, fptr);
      fflush(fptr);
      printf("\n*** Loaned book %d, %s to student with ID%d***\n\n", book,currentBook.title,student);
    };
  }
  fclose(fptr);
}

// unassign a book from a student
void returnBook(int student, int book)
{
  struct loanRecord currentLoan;
  struct bookRecord currentBook;
  struct studentRecord currentStudent;
  FILE *fptr;

  if ((fptr = fopen("loans.bin","ab")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }
  while (fread(&currentLoan, sizeof(struct loanRecord), 1, fptr))
  {
    if(currentLoan.studentId==student && currentLoan.bookId==book)
    {
      currentLoan.returned=true;
      fseek(fptr, -sizeof(struct loanRecord),SEEK_CUR);
      fwrite(&currentLoan, sizeof(struct loanRecord), 1, fptr);
      fflush(fptr);
      printf("removed the loan\n");
    };
  }
    fclose(fptr);


  if ((fptr = fopen("books.bin","rb+")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if(currentBook.bookId==book)
    {
      currentBook.state=BOOK_AVAILABLE;
      fseek(fptr, -sizeof(struct bookRecord),SEEK_CUR);
      fwrite(&currentBook, sizeof(struct bookRecord), 1, fptr);
      fflush(fptr);
      printf("\n*** Returned book %d, %s from student with ID%d***\n\n", book,currentBook.title,student);
    };
  }
  fclose(fptr);
}

// retrieve a book record by title and author
void searchBook(int id, char * title)
{
  struct bookRecord currentBook;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if(currentBook.bookId==id)
    {
      strcpy(title,currentBook.title);
    }
  }
  fclose(fptr);
}

void searchStudent(int id, char * firstName, char * lastName)
{
  struct studentRecord currentStudent;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentStudent, sizeof(struct studentRecord), 1, fptr))
  {
    if(currentStudent.studentId==id)
    {
      strcpy(firstName,currentStudent.firstName);
      strcpy(lastName,currentStudent.lastName);
    }
  }
  fclose(fptr);
}

bool validateUser(char firstName[], char password[])
{
  struct studentRecord currentStudent;
  FILE *fptr;

  if ((fptr = fopen("students.bin","rb")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentStudent, sizeof(struct studentRecord), 1, fptr))
  {
    if(strcmp(currentStudent.firstName,firstName)==0 ||strcmp (currentStudent.password,password)==0)
    {
      fclose(fptr);
      return true;
    }
  }
  fclose(fptr);
}

// list books by genre
void listByAuthor(char author[])
{
  struct bookRecord currentBook;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if (currentBook.state!=BOOK_DELETED)
    {
      if(strcmp(currentBook.author,author)==0||strcmp(author,"All")==0)
      {
        printf("ID: %d\n", currentBook.bookId);
        printf("Title: %s\n", currentBook.title);
        printf("Author: %s\n", currentBook.author);
        printf("Year: %s\n", currentBook.year);
        printf("Genre: %s\n", currentBook.genre);
        printf("Status: %s\n", currentBook.state==BOOK_AVAILABLE?"Available": "On Loan");
      }
    }
}
}

// list books by genre
void listByGenre(char genre[])
{
  struct bookRecord currentBook;
  FILE *fptr;

  if ((fptr = fopen("books.bin","rb")) == NULL){
      printf("Error! opening books file");
      exit(1);
  }

  while (fread(&currentBook, sizeof(struct bookRecord), 1, fptr))
  {
    if (currentBook.state!=BOOK_DELETED)
    {
      if(strcmp(currentBook.genre,genre)==0||strcmp(genre,"All")==0)
      {
        printf("ID: %d\n", currentBook.bookId);
        printf("Title: %s\n", currentBook.title);
        printf("Author: %s\n", currentBook.author);
        printf("Year: %s\n", currentBook.year);
        printf("Genre: %s\n", currentBook.genre);
        printf("Status: %s\n", currentBook.state==BOOK_AVAILABLE?"Available": "On Loan\n\n");
      }
    }
  }
  fclose(fptr);
}

void listAllBooks()
{
  listByGenre("All");
}


void main()
{
int id;
int bookId;
bool quitting = false;
bool valid =false;
int choice;
char staffPass[100];
char title[50];
char firstName[50];
char lastName[50];
char password[50];
char author[50];
char genre [50];
staffPass=="project";
  do{
    printf("\n** Welcome would you like to login as a student or staff **\n");
    printf("1.Student\n2.Staff\n");
    scanf("%i",&choice);
    switch(choice){
      case 1:
        system("cls");
        printf("\nChose whether you would like to login or register: \n");
        printf("1.Login\n2.Register\n");
        scanf("%i",&choice);
        switch(choice){
          case 1:
              printf("Please enter your first name: \n");
              scanf("%s",&firstName);
              printf("Please enter your password: \n");
              scanf("%s",&password);
              valid ==validateUser(firstName,password);
              if (valid=true){
                do{
                  printf("\nSelect a function: \n");
                  printf("1.Return a book\n2.Borrow a book\n3.list all books\n4.Search by genre\n5.Search for a book\n6.Quit\n");
                  scanf("%i",&choice);
                  switch(choice){
                    case 1:
                      printf("please enter your ID\n");
                      scanf("%i",&id);
                      printf("please enter the books's ID\n");
                      scanf("%i",&bookId);
                      returnBook(id,bookId);
                      break;
                    case 2:
                      printf("please enter your ID\n");
                      scanf("%i",&id);
                      printf("please enter the books's ID\n");
                      scanf("%i",&bookId);
                      loanBook(id,bookId);
                      break;
                    case 3:

                      listAllBooks();
                      break;
                    case 4:
                    printf("please enter the books's genre\n");
                    scanf("%s",&genre);
                      listByGenre(genre);
                      break;
                    case 5:
                      printf("please enter the books's ID\n");
                      scanf("%i",&id);
                      searchBook(id,title);
                      printf("ID%i, belongs to %s",id,title);
                      break;
                    case 6:
                      quitting =true;
                      exit(1);
                    default:
                      printf("invalid responce\n");
                      break;
                  }
                }while(quitting =true);

              }else
              printf("Data doesn't match\n");


            break;
          case 2:
          printf("\nplease enter the admin password to continue: ");
          scanf("%s",&staffPass);

            if (strcmp(staffPass,"project")==0){
              addStudent();
            }else
              printf("**incorrect password**\n");
            break;
          default:
            printf("invalid responce\n");
            break;
        }
        break;

      case 2:
      printf("\nplease enter the admin password to continue: ");
      scanf("%s",&staffPass);

        if (strcmp(staffPass,"project")==0){
          printf("as\n");
          do{
            printf("\nSelect a function: \n");
            printf("1.Add a student\n2.Add a Book\n3.Delete a Book\n4.Search for a student\n5.Search for a book\n6.Quit\n");
            scanf("%i",&choice);
            switch(choice){
              case 1:
                addStudent();
                break;
              case 2:
                addBook();
                break;
              case 3:
                printf("Please enter the ID of the book you wish to delete: \n");
                scanf("%i",&choice);
                deleteBook(choice);
                break;
              case 4:
                printf("please enter the student's ID\n");
                scanf("%i",&id);
                searchStudent(id,firstName,lastName);
                printf("ID%i, belongs to %s",id,firstName);
                break;
              case 5:
                printf("please enter the books's ID\n");
                scanf("%i",&id);
                searchBook(id,title);
                printf("ID%i, belongs to %s",id,title);
                break;
              case 6:
                quitting = true;
                break;
              default:
                printf("invalid responce\n");
                break;
            }
          }while (quitting = true);

        }else
          printf("Incorrect Password\n");
          break;
      default:
        printf("invalid responce\n");
        break;
    }

  }while (quitting =true);{
    exit(1);
  }
}
